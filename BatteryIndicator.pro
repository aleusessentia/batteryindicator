TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += $$PWD/source/main.cpp

RESOURCES += $$PWD/qml/qml.qrc \
    $$PWD/images/images.qrc \
    $$PWD/fonts/fonts.qrc 

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
