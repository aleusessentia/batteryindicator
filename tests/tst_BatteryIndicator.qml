import QtQuick 2.8
import QtTest 1.1

Item {
    property real dp: 1.0

    BatteryIndicator {
        id: testBI
    }

    BatteryIndicator {
        id: sample
    }

    TestCase {
        id: testCase
        name: "BatteryIndicatorTests"
        when: windowShown

        function test_bigValue() {
            testBI.value = 2.0;
            sample.value = 1.0;
            var testImage = grabImage(testBI);
            var sampleImage = grabImage(sample);
            verify(testImage.equals(sampleImage), "value > 0.0");
        }

        function test_negativeValue() {
            testBI.value = -2.0;
            sample.value = 0.0;
            var testImage = grabImage(testBI);
            var sampleImage = grabImage(sample);
            verify(testImage.equals(sampleImage), "value < 0.0");
        }
    }
}
