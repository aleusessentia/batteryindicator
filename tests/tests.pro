TEMPLATE = app
TARGET = tst_batery_indicator
CONFIG += warn_on qmltestcase
SOURCES += tests.cpp

RESOURCES += $$PWD/../images/images.qrc

OTHER_FILES += tst_BatteryIndicator.qml
IMPORTPATH += $$PWD/../qml
