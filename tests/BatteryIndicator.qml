import QtQuick 2.8
import QtQuick.Window 2.2
import QtTest 1.1

//------------------------------------------------------------------------------
// Компонент индикатора загрузки
//------------------------------------------------------------------------------
MouseArea {
    id: _batteryIndicator

    // Значение от 0 до 1
    property real value: 0.0

    // Критическое значение заряда
    property real criticalValue: 0.1

    // Флаг подзарядки устройства
    property bool charging: false

    property string toolTipLabel: ""
    property string toolTipLabelRight: ("%1\%").arg(Math.round(value*100))

    width: 44*dp
    height: 25*dp
    state: statesItem.state // Для тестов
    
    Image {
        id: emptyImage

        source: "qrc:/images/battery-empty.png"
        width: 20*dp
        anchors.centerIn: parent
    }

    //--------------------------------------------------------------------------
    Item {
        id: chargeItem

        // Значения взяты из размеров картинки + компенсация визуального представления
        readonly property int minWidth: 2*dp + 1*dp
        readonly property int maxWidth: 16*dp
        readonly property real deltaWidth: maxWidth - minWidth

        property int currentWidth: value > 0.0
                                   ? Math.min(maxWidth, minWidth + Math.max(0, value)*deltaWidth)
                                   : 0.0

        clip: true
        width: currentWidth
        anchors {
            top: emptyImage.top
            left: emptyImage.left
            bottom: emptyImage.bottom
        }

        Image {
            id: chargeImage

            source: "qrc:/images/battery-charging.png"
            width: 20*dp
        }
    }

    //--------------------------------------------------------------------------
    Item {
        id: statesItem

        state: "normal"
        states: [
            State {
                name: "charged"
                when: value >= 1.0

                PropertyChanges {
                    target: _batteryIndicator
                    toolTipLabel: qsTr("CHARGED")
                }
                PropertyChanges {
                    target: chargeItem
                    width: emptyImage.width
                    opacity: 1.0
                }
                PropertyChanges {
                    target: chargeImage
                    source: "qrc:/images/battery-charged.png"
                }
            }
            ,State {
                name: "charging"
                when: charging

                PropertyChanges {
                    target: _batteryIndicator
                    toolTipLabel: qsTr("CHARGING")
                }
                PropertyChanges {
                    target: chargeItem
                    opacity: 1.0
                }
                PropertyChanges {
                    target: chargeAnimation
                    running: true
                }
            }
            ,State {
                name: "critical"
                when: value <= criticalValue

                PropertyChanges {
                    target: _batteryIndicator
                    toolTipLabel: qsTr("CRITICAL")
                }
                PropertyChanges {
                    target: criticalAnimation
                    running: true
                }
            }
            ,State {
                name: ""

                PropertyChanges {
                    target: _batteryIndicator
                    toolTipLabel: qsTr("NORMAL?")
                }
                PropertyChanges {
                    target: chargeItem
                    opacity: 1.0
                }
                PropertyChanges {
                    target: chargeItem
                    width: currentWidth
                }
            }
        ]
    }

    //--------------------------------------------------------------------------
    // Анимация зарядки устройства
    //--------------------------------------------------------------------------
    SequentialAnimation {
        id: chargeAnimation

        loops: Animation.Infinite

        NumberAnimation {
            target: chargeItem
            property: "width"
            from: 0.0
            to: chargeItem.currentWidth
            duration: 1000
        }

        NumberAnimation {
            target: chargeItem
            property: "width"
            from: chargeItem.currentWidth
            to: 0.0
            duration: 1000
        }
    }

    //--------------------------------------------------------------------------
    // Анимация критичности заряда
    //--------------------------------------------------------------------------
    SequentialAnimation {
        id: criticalAnimation

        loops: Animation.Infinite

        NumberAnimation {
            target: chargeItem
            property: "opacity"
            from: 0.0
            to: 1.0
            duration: 300
        }

        NumberAnimation {
            target: chargeItem
            property: "opacity"
            from: 1.0
            to: 0.0
            duration: 300
        }
    }

    //--------------------------------------------------------------------------
    onClicked: {
        toolTip.show(_batteryIndicator);
    }
}
