import QtQuick 2.8
import QtQuick.Window 2.2

Item {
    id: _toolTip

    property var currentItem

    x: toolBar.x - width
    y: currentItem != null
       ? Math.min(parent.height- height, Math.max(0
          , currentItem.mapToItem(toolBar,0,0).y + 0.5*(currentItem.height - height)))
       : 0.0
    width: 300
    height: 40*dp
    visible: false
    clip: true

    Rectangle {
        id: background

        radius: 5*dp
        color: "#4d2f31"
        anchors {
            fill: parent
            rightMargin: -radius
        }
    }

    Text {
        id: label

        text: currentItem != null
              ? currentItem.toolTipLabel
              : ""
        color: "#7d888a"
        font.pixelSize: 11*dp
        elide: Text.ElideRight
        anchors {
            left: parent.left
            right: rightLabel.left
            leftMargin: 20*dp
            rightMargin: 10*dp
            verticalCenter: parent.verticalCenter
        }
    }

    Text {
        id: rightLabel

        color: "white"
        text: currentItem != null
              ? currentItem.toolTipLabelRight
              : ""
        font.pixelSize: 11*dp
        anchors {
            right: parent.right
            rightMargin: 20*dp
            verticalCenter: parent.verticalCenter
        }
    }

    //--------------------------------------------------------------------------
    function show(item) {
        currentItem = item;
        visible = true;
    }

    //--------------------------------------------------------------------------
    function hide() {
        visible = false;
    }
}
