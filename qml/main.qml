import QtQuick 2.8
import QtQuick.Window 2.2
import QtQuick.Controls 2.1 as Ctrl

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("BatteryIndicator")
    color: "white"

    //--------------------------------------------------------------------------
    ToolTip {
        id: toolTip
    }

    //--------------------------------------------------------------------------
    Rectangle {
        id: toolBar

        width: 44*dp
        anchors {
            top: parent.top
            right: parent.right
            bottom: parent.bottom
        }
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#3c2728" }
            GradientStop { position: 0.45; color: "#322325" }
            GradientStop { position: 1.0; color: "#161617" }
        }

        //----------------------------------------------------------------------
        BatteryIndicator {
            id: batteryIndicator

            value: 1.0
            anchors {
                top: parent.top
                topMargin: 55*dp
                horizontalCenter: parent.horizontalCenter
            }
        }
    }

    //--------------------------------------------------------------------------
    // Debug!!! Для скрытия ToolTip.
    //--------------------------------------------------------------------------
    MouseArea {
        id: hideMouseArea

        anchors.fill: parent
        enabled: toolTip.visible
        onClicked: {
            toolTip.hide()
        }
    }

    //--------------------------------------------------------------------------
    // Элементы управления для отладки индикатора зарядки
    //--------------------------------------------------------------------------
    Column {
        id: debugControls

        Ctrl.CheckBox {
            text: qsTr("Charging")

            onCheckedChanged: {
                batteryIndicator.charging = checked;
            }
        }

        Ctrl.Slider {
            value: batteryIndicator.value
            from: 0
            to: 1.0

            onValueChanged: {
                batteryIndicator.value = value
            }
        }
    }
}
