#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFontDatabase>
#include <QScreen>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QFontDatabase::addApplicationFont(":/fonts/montserrat-light.ttf");
    app.setFont(QFont("Montserrat Light"));

    float dp = app.screens().first()->devicePixelRatio();
    engine.rootContext()->setContextProperty("dp", dp);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
